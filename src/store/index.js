import {createStore} from 'vuex';

export default createStore({
    state: {
        formData: [],
    },
    mutations: {
        setFormData(state, data) {
            state.formData.push(data);
        },
        deleteUser(state, index) {
            state.formData.splice(index, 1);
        },
        updateUserFIO(state, payload) {
            const {index, newFIO} = payload;
            if (state.formData[index]) {
                state.formData[index].fio = newFIO;
            }
        },
    },
    actions: {
        submitForm({commit}, formData) {
            commit('setFormData', formData);
        },
        changeUserFIO({commit, state}, payload) {
            const {index, newFIO} = payload;
            if (index >= 0 && index < state.formData.length) {
                commit('updateUserFIO', {index, newFIO});
            }
        },
    },
    getters: {
        getFormData(state) {
            return state.formData;
        },
    },
});
